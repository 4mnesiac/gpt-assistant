import { unlink } from 'fs/promises';

export async function removeFile(path: string) {
  try {
    await unlink(path);
  } catch (error) {
    console.log(`Mp3 convert error: ${(error as Error).message}`);
  }
}
