import {
  Configuration,
  OpenAIApi,
  ChatCompletionRequestMessage,
  ChatCompletionRequestMessageRoleEnum,
} from 'openai';
import config from 'config';
import { PathLike, createReadStream } from 'fs';

export type Messages = ChatCompletionRequestMessage[];
class OpenAI {
  roles = ChatCompletionRequestMessageRoleEnum;
  private gpt: OpenAIApi;
  constructor(apiKey: string) {
    const configuration = new Configuration({
      apiKey,
    });
    this.gpt = new OpenAIApi(configuration);
  }

  async chat(messages: Messages) {
    try {
      const response = await this.gpt.createChatCompletion({
        model: 'gpt-3.5-turbo',
        messages,
      });
      return response.data.choices[0].message;
    } catch (error) {
      console.log(`Chat error: ${(error as Error).message}`);
      throw error;
    }
  }

  async transcript(filepath: PathLike) {
    try {
      const response = await this.gpt.createTranscription(
        createReadStream(filepath),
        'whisper-1'
      );
      return response.data.text;
    } catch (error) {
      console.log(`Transcript error: ${(error as Error).message}`);
      throw error;
    }
  }
}

export const openai = new OpenAI(config.get('OPENAI_KEY'));
