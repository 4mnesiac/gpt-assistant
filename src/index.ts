import { Context, Telegraf, session } from 'telegraf';
import { message } from 'telegraf/filters';
import { code } from 'telegraf/format';
import config from 'config';
import { audio } from './modules/Audio.js';
import { Messages, openai } from './modules/Openai.js';

console.log(config.get('TEST_ENV'));

const INIT_SESSION = {
  messages: [],
};
const sessionMiddleware = session();

interface SessionData {
  messages: Messages;
}

interface BotContext extends Context {
  session?: SessionData;
}
const bot = new Telegraf<BotContext>(config.get('TELEGRAM_TOKEN'));

bot.use(sessionMiddleware);

bot.command('new', async ctx => {
  ctx.session = INIT_SESSION;
  await ctx.reply('Жду вашего голосового или текстового сообщения');
});

bot.on(message('voice'), async ctx => {
  ctx.session ??= INIT_SESSION;
  try {
    await ctx.reply(code('Я Вас услышал. Формирую ответ...'));
    const link = await ctx.telegram.getFileLink(ctx.message.voice.file_id);
    const userId = String(ctx.message.from.first_name);
    const oggPath = await audio.create(link.href, userId);
    const mp3Path = await audio.toMp3(oggPath as string, userId);

    const text = await openai.transcript(mp3Path);
    await ctx.reply(code(`Ваш запрос: ${text}`));

    ctx.session.messages.push({ role: openai.roles.User, content: text });
    const response = await openai.chat(ctx.session.messages);

    ctx.session.messages.push({
      role: openai.roles.Assistant,
      content: response!.content,
    });

    await ctx.reply(response!.content);
  } catch (error) {
    console.log(`Voice message error: ${(error as Error).message}`);
  }
});
bot.on(message('text'), async ctx => {
  ctx.session ??= INIT_SESSION;
  try {
    await ctx.reply(code('Запрос принят. Формирую ответ...'));

    ctx.session.messages.push({
      role: openai.roles.User,
      content: ctx.message.text,
    });
    const response = await openai.chat(ctx.session.messages);

    ctx.session.messages.push({
      role: openai.roles.Assistant,
      content: response!.content,
    });

    await ctx.reply(response!.content);
  } catch (error) {
    console.log(`Text message error: ${(error as Error).message}`);
  }
});

bot.on(message('text'), async ctx => {
  await ctx.reply(JSON.stringify(ctx.message, null, 2));
});

bot.command('start', async ctx => {
  ctx.session = INIT_SESSION;
  await ctx.reply('Жду вашего сообщения');
});

bot.launch();

process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));
